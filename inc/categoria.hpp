#ifndef CATEGORIA_HPP
#define CATEGORA_HPP

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "produto.hpp"

using namespace std;

class Categoria{
    private: 
    string nome;
    vector <Produto *> produtos;

    public:
    Categoria();
    ~Categoria();
    Categoria(string nome);
    Categoria(string nome, vector<Produto *> produtos);
    Categoria(vector <Produto*> produtos);

    void set_categoria(string nome);
    string get_categoria();
    void set_produto(string codigo);
    void set_produtos(vector<Produto *> produtos);
    void set_produto(Produto * produto);
    vector<Produto *> get_produto();

    bool novacategoria(string categoria);
    void cadastro_codigo();
    bool rArquivo(string categoria); //read arquivo 
};


#endif