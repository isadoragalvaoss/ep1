#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <vector>
#include <string>
#include "produto.hpp"
#include "compras.hpp"

using namespace std;

class Cliente{
    private: 
    string nome;
    string cpf;
    string socio;
    float valor;
    bool is_socio;
    float total;
    Compras compras;

    public:
    Cliente();
    Cliente(string nome, string cpf);
    ~Cliente();
    void set_nome(string nome);
    string get_nome();
    void set_cpf(string cpf);
    string get_cpf();
    void set_socio(bool is_socio);
    bool get_socio();
    virtual void imprime();
    void set_total (float total);
    float  get_total();

    Compras get_compras();

    void imprimeclientes(vector<Cliente *> cliente);
    void cadastrocliente();
    Cliente * rArquivo (string arquivo);
    string conf_cpf(string cpf);

    void compracliente(string codigo,int quant);
    bool comprafinal();
    virtual void final();   
};

#endif