#ifndef CLIENTE_SOCIO_HPP
#define CLIENTE_SOCIO_HPP

#include <iostream>
#include <vector>
#include <string>

#include "cliente.hpp"
#include "compras.hpp"

using namespace std;

class Csocio: public Cliente{
    private:
    float desconto;
    public:
    Csocio();
    ~Csocio();
    Csocio(Cliente *cliente);

    void imprime();
    void set_desconto(float desconto);
    float get_desconto();
    void valorfinal();
};

#endif