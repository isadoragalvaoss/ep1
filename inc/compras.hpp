#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <iostream>
#include <vector>
#include <string>

#include "produto.hpp"
using namespace std;

class Compras{
    private:
    vector<Produto *> produtos;
    vector<int> quants;

    public:
    Compras();
    ~Compras();

    void set_produto(Produto * produto);
    vector<Produto *> get_produto();
    void set_quants(int quant);
    vector<int> get_quants();
    void somacompras(string codigo, int quant);
    float valortotalcompras(float desct);
    bool conf_estoque();
};

#endif