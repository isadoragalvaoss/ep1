#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include <vector>
#include <iostream>
#include <string>
#include "produto.hpp"

using namespace std;

class Estoque{
    public:
    Estoque();
    ~Estoque();

    void set_produto(string codigo,string nome,int quant,float valor);
    void novaquantidade(string codigo,int quant);
    void estoquecadastroproduto(Produto * produto);
    string existeproduto(string codigo);
};

#endif