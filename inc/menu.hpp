#ifndef MENU_HPP
#define MENU_HPP

#include "produto.hpp"
#include "estoque.hpp"
#include "categoria.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"
#include <iostream>
#include <string>
using namespace std;

    
 class Menu{    
  
  private:
    // Recomendacao *recomendacao;
    Estoque *estoque;
    Categoria *categorias;
    vector<Cliente *> clientes;
  
  
  public:
    void limpaTela();
    void menuInicial ();
    void modoEstoque();
    void modoVenda();
    void modoRecomendacao();
    void menucadastroclientes(string cpf);
    void menucadastrocategoria();
    void menucadastroproduto();
    int   cadastro();
    bool cadastroexiste(string arquivo, string teste);
    void compra(int conf);
    void novoestoque();
    void set_cliente(string nome, string cpf);
    void modocadastraprodutos();

    Menu();
    ~Menu();
 };

 #endif 