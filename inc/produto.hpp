#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <string>

using namespace std;

class Produto{
    private:
    string codigo;
    string nome;
    int quant;
    float valor;

    public:
    Produto();
    Produto(string codigo, string nome,int quant, float valor);
    ~Produto();

    void set_codigo(string codigo);
    string get_codigo();
    void set_nome(string nome);
    string get_nome();
    void set_quant(int quant);
    int get_quant();
    void set_valor(float valor);
    float get_valor();

    void imprime();
    
    void cadastro_produto();
    string conf_produto(string codigo);
    Produto * rArquivo(string arquivo);
    bool conf_estoque(int quant);

};

#endif 