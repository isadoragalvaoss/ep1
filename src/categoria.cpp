#include <fstream>
#include "categoria.hpp"

using namespace std;

Categoria:: Categoria(){
}
Categoria:: ~Categoria(){
}
Categoria:: Categoria (string nome){
    set_categoria(nome);
}
Categoria:: Categoria (string nome, vector<Produto *> produtos){
    set_categoria(nome);
    set_produtos(produtos);
}
Categoria:: Categoria (vector <Produto *> produtos){
    set_categoria("geral");
    set_produtos(produtos);
}

void Categoria :: set_categoria(string nome) {
    this->nome=nome;
}
string Categoria:: get_categoria(){
    return this->nome;
}

void Categoria:: set_produto(string codigo){
    Produto produto;
    string a;
    a=produto.conf_produto(codigo);

    if(a=="./arq/produtos/"+codigo+".txt")
    {
        set_produto(produto.rArquivo(a));
        cout<< "---produto "<<produtos[produtos.size()-1]->get_nome()<< " encontrado---"<< endl;
    }
    else {
        cout<<"erro: produto nao encontrado" << endl;
    }
}

void Categoria:: set_produto (Produto * produto){
    produtos.push_back(produto);
}
void Categoria:: set_produtos (vector<Produto*> produtos){
    this->produtos= produtos;
}
vector<Produto *> Categoria:: get_produto(){
    return produtos;
}

bool Categoria:: novacategoria(string categoria){
    ofstream arquivos;

    if (rArquivo(categoria)) {
        arquivos.open("./arq/categorias/categoria.txt",ios::app); //ios app para manter

        if (arquivos.is_open()){
            arquivos<< get_categoria()<< endl;
            arquivos.close();
            cout<< "---categoria "<<categoria<<" cadastrada---" << endl;

        }
        return true;
    }
    else { 
        cout<< "---categoria ja cadastrada----"<< endl;
        return false;
    }
}

void Categoria:: cadastro_codigo(){
    ofstream arquivos;

    arquivos.open("./arq/categorias/categorias/"+get_categoria()+".txt",ios::trunc);

    if(arquivos.is_open()) {
        for (Produto * produto: produtos){
            arquivos<< produto -> get_codigo() << endl;
        }
        arquivos.close();
    }
}

bool Categoria:: rArquivo(string categoria){
    ifstream arquivoe;
    string texto;

    arquivoe.open("./arq/categorias/categoria.txt");

    if(arquivoe.is_open()){
        while (arquivoe.eof() != true){
            getline(arquivoe,texto);
            if (texto==categoria){
                return false;
            }
        }
        arquivoe.close();
    }
    return true;
}