#include <iostream>
#include <string>
#include <fstream>
#include "cliente.hpp"

using namespace std;

Cliente:: Cliente(){
}
Cliente:: ~Cliente(){
}
Cliente:: Cliente(string nome, string cpf){
    set_nome(nome);
    set_cpf(cpf);
    set_socio(false);
    set_total(0);
    cadastrocliente();
}

void Cliente:: set_socio(bool is_socio){
    this->is_socio= is_socio;
}
bool Cliente:: get_socio(){
    return is_socio;
}

void Cliente:: set_total(float total){
    this->total=total;
}
float Cliente:: get_total(){
    return total;
}

Compras Cliente::get_compras(){
    return this->compras;
}

void Cliente:: set_nome(string nome){
    this->nome=nome;
}
string Cliente::get_nome(){
    return this->nome;
}

void Cliente:: set_cpf(string cpf){
    this->cpf=cpf;
}
string Cliente:: get_cpf(){
    return this->cpf;
}

void Cliente:: imprime(){
    cout<<"nome: "<< get_nome()<<endl;
    cout<<"cpf: "<< get_cpf()<<endl;
    cout<<"socio: ";
    if (get_socio()== 1){
        cout<<"sim"<<endl;
    }
    else{
        cout<<"nao"<<endl;
    }
    cout<< "total: "<<get_total()<<endl;
}

void Cliente:: imprimeclientes(vector<Cliente *> cliente){
    cout<< "-------clientes cadastrados-------"<<endl;

    for(Cliente *cliente: cliente){
        cliente->imprime();
    }
}

void Cliente:: cadastrocliente(){
    ofstream arquivos;
    arquivos.open("./arq/clientes/"+get_cpf()+".txt",ios::trunc);

    if(arquivos.is_open()){
        arquivos<< get_nome()<<endl;
        arquivos<< get_cpf()<<endl;
        arquivos<< get_socio()<< endl;
        arquivos<<get_total()<<endl;
        arquivos.close();
    }
    arquivos.close();
}

Cliente * Cliente:: rArquivo (string arquivo){
    ifstream arquivoe;
    Cliente * clientes= new Cliente;
    string texto;
    int cont=0;
    float total2;

    arquivoe.open(arquivo,ios::in);
    if(arquivoe.is_open()){
        getline(arquivoe,texto);
        clientes->set_nome(texto);
        cont++;
        getline(arquivoe,texto);
        clientes->set_cpf(texto);
        cont++;
        getline(arquivoe,texto);
        if(texto=="1"){
            clientes->set_socio(true);
        }
        else{
            clientes->set_socio(false);
        }
        cont++;
        getline(arquivoe,texto);
        total2= stof(texto);
        clientes->set_total(total2);
        
        arquivoe.close();
        
    }
   return clientes; 
}

string Cliente:: conf_cpf(string cpf){
    ifstream arquivo;
    string a;

    a="./arq/clientes/"+cpf+".txt";

    arquivo.open(a);

    if(arquivo.is_open()){
        return a;
    }
    arquivo.close();
    

    return "***cpf nao cadastrado***";
}

void Cliente:: compracliente(string codigo,int quant){
    compras.somacompras(codigo,quant);
}

bool Cliente:: comprafinal(){
    return compras.conf_estoque();
}

void Cliente:: final(){
    set_total(get_total()+ compras.valortotalcompras(0.0));
}

