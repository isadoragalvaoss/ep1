#include <iostream>
#include <fstream>
#include <cstring>

#include "cliente_socio.hpp"
using namespace std;

Csocio:: Csocio(){
}
Csocio:: ~Csocio(){
}
Csocio:: Csocio(Cliente *cliente){
    set_nome(cliente->get_nome());
    set_cpf(cliente->get_cpf());
    set_socio(cliente->get_socio());
    set_total(cliente->get_total());

    if (get_total()>=100){
        set_desconto(0.15);
    }
}

void Csocio:: set_desconto(float desconto){
    this->desconto=desconto;
}
float Csocio:: get_desconto(){
    return this-> desconto;
}

void Csocio:: valorfinal(){
    set_total(get_total()+(get_compras().valortotalcompras(get_desconto()))); 
}

void Csocio:: imprime(){
    cout<< "cliente: "<< get_nome()<<endl;
    cout<<"cpf: "<< get_cpf()<< endl;
    cout<< "socio: sim"<< endl;
    cout<< "desconto de 15%= "<< get_desconto()*100<<"%"<<endl;
    cout<<"valor final: "<< get_total()<<endl;
}