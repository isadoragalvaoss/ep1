#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "compras.hpp"
using namespace std;

Compras:: Compras(){
}
Compras:: ~Compras(){
}

void Compras:: set_produto(Produto *produto){
    this->produtos.push_back(produto);
}
vector<Produto *> Compras:: get_produto(){
    return this-> produtos;
}

void Compras:: set_quants(int quant){
    this->quants.push_back(quant);
}
vector<int> Compras:: get_quants(){
    return this-> quants;
}


void Compras:: somacompras(string codigo,int quant){
    string arquivo;
    Produto produto;

    arquivo= produto.conf_produto(codigo);

    if(arquivo != "./arq/produtos/"+codigo+".txt"){
        cout<< "***produto nao encontrado***"<<endl;
    }
    else{
        set_produto(produto.rArquivo(arquivo));
        cout<< "***produto ";
        produtos[produtos.size()-1]->get_nome();
        cout<< " adicionado as compras*** ";
        set_quants(quant);
    }
}

float Compras:: valortotalcompras(float desct){
    int cont=0;
    float valor=0;

    cout<< "=============================="<<endl;
    cout<< "          COMPRAS"<<endl;
    
    for(Produto *produto: produtos){
        cout<<"  nome: ";
        cout<< produto-> get_nome()<<endl;
        cout<<"  quantidade: ";
        cout<<quants[cont]<< endl;
        cout<< "  valor: ";
        cout<< produto->get_valor()<<endl;
        cout<<"  total: ";
        cout<< produto->get_valor()*quants[cont]<<endl;
        valor += (produto->get_valor()*quants[cont]);
        cont++;

    }
    cout<< "-------------------"<<endl;
    cout<<"  valor total: "<< valor<<endl;
    cout<<"  desconto: "<<(desct*valor)<<endl;
    cout<<"  valor final: "<<(1-desct)*valor<<endl;
    cout<< "=============================="<<endl;


    return (1-desct)*valor;
}

bool Compras:: conf_estoque(){
    int cont=0;
    vector<int> i;
    vector<bool> ok;

    for(Produto *produto: produtos){

        if(produto->conf_estoque(quants[cont])) {
            ok.push_back(true);
        }
        else{
            ok.push_back(false);
        }
        cont++;
    }

    for (bool x: ok){
        if(x==false){
            return false;
        }
    }

    for (Produto *produto: produtos){
        produto-> cadastro_produto();
    }

    return true;
}