#include <vector>
#include <unistd.h>
#include <iostream>
#include <string>
#include "menu.hpp"

using namespace std;

    string getString(){
      string valor;
      getline(cin,valor);
      return valor;
    }

template <typename T1>
T1 getInput(){
  while(true){
    T1 valor;
    cin>>valor;
    if(cin.fail()){
      cin.clear();
      cin.ignore(32767,'\n');
      cout<<"invalido! tente novamente"<< endl;
      }
    else {
      cin.ignore(32767,'\n');
      return valor;
    }  
  }
}    
    Menu:: Menu(){
    }
    Menu:: ~Menu(){
    
    }
    void Menu:: limpaTela(){
cout << "\033[2J\033[1;1H"<<endl;
}

void Menu:: menuInicial () { 
  int opcao = 0;
  string nomeCliente;
  
 while (opcao<1 || opcao>3){
    limpaTela();
    cout<< "============================="<<endl;
    cout<< "  Bem vindo ao mercado" <<endl;
    cout<< "  1- modo venda" << endl;
    cout<< "  2- modo estoque"<< endl;
    cout<< "  3- modo recomendação"<< endl;
    
    cout<< "  Escolha uma e tecle enter: "<<endl;
    cout<< "=============================="<<endl;
    // cin >> opcao;
    opcao= getInput<int>();
    
  if(opcao==1) {
    limpaTela();
    modoVenda();
  }
  else if(opcao==2){
    limpaTela();
    modoEstoque();
  }
  else if (opcao==3){
    limpaTela();
    modoRecomendacao();
  }
  }
   }

void Menu:: modoEstoque(){
    int opc;
    Produto p;
    cout<< "=============================="<<endl;
    cout<< "       MODO ESTOOQUE"<<endl;
    cout<< "  1- cadastrar produto" << endl;
    cout<< "  2- atualizar estoque"<< endl;
    cout<< "  3- cadastrar categoria"<< endl;
    
    cout<< "  4-voltar" << endl;
    

    cout<< "Escolha uma e tecle enter: "<<endl;
    
    // cin>> opc;
    opc= getInput<int>();

    if (opc==1){
      menucadastroproduto();
    }
    else if (opc==2){
      novoestoque();
    }
    else if (opc ==3){
      menucadastrocategoria();
    }
    else if (opc == 4){
      menuInicial();
    }
}

  
void Menu:: menucadastroproduto(){
  string nome,codigo,valor;
  // float valor;
  int quant;

    cout<< "=============================="<<endl;
    cout<< "   preencha com os dados do produto: "<< endl;
    cout<< "        codigo:  ";
    codigo= getString();
    // cin>>codigo;
    cout<< "        nome:    ";
    nome=getString();
    cout<< "        quantidade:  ";
    quant = getInput<int>();
    cout<< "        valor: ";
    // valor=getInput<float>();
    valor= getString();
    float valor2= stof(valor);
    cout<< "=============================="<<endl;
    estoque-> set_produto(codigo,nome,quant,valor2);
    cout<< "pressione ENTER";
    nome=getString();
    limpaTela();
    modoEstoque();
}

void Menu::novoestoque(){
  string codigo;
  int quant;
  cout<< "=============================="<<endl;
  cout<< "  insira o codigo do produto:  "<< endl;
  codigo=getString();
  cout<< "  insira a quantidade do produto:  "<< endl;
  quant= getInput<int>();
  cout<< "=============================="<<endl;

  estoque->novaquantidade(codigo,quant);
  cout<< "pressione ENTER";
  codigo = getString();
  limpaTela();
  modoEstoque();
}

void Menu:: menucadastrocategoria(){
  string categoria;
  string codigo;
  string opc="s";
  categorias = new Categoria();

  cout<< "=============================="<<endl;
  cout<<"   insira nova categoria:  "<<endl;
  categoria = getString();
  categorias = new Categoria(categoria);
  cout<< "=============================="<<endl;

  if (categorias->novacategoria(categoria)){
      while(opc=="s"||opc=="S"){
      
        cout<<"    categoria: "<<categoria<<endl;
        cout<<"    codigo dos produtos: ";
        codigo= getString();
        categorias->set_produto(codigo);

        cout<<"continuar acrescentando?"<<endl;
        cout<<"[s/n]";
        opc=getString();
      }

      categorias->cadastro_codigo();
      categorias->~Categoria();
  }

  cout<< "pressione ENTER";
  categoria= getString();
  limpaTela();
  modoEstoque();
  
}

void Menu:: modoVenda(){
    
    int c;
    cout<< "=============================="<<endl;
    cout<< "         MODO VENDA         "<<endl;
    cout<< "          cpf: ";
    
    c= cadastro();
    compra(c);

}

int Menu:: cadastro(){
  string arquivo;
  string cpf;
  int a=0;
  clientes.push_back(new Cliente());

  cpf= getString();

  arquivo= clientes[clientes.size()-1]->conf_cpf(cpf);

  if(cadastroexiste(arquivo,"./arq/clientes/"+cpf)){
    clientes[clientes.size()-1]=clientes[clientes.size()-1]->rArquivo(arquivo);

      if(clientes[clientes.size()-1]->get_socio() || clientes[clientes.size()-1]->get_total()>=100){
        clientes.push_back(new Csocio(clientes[clientes.size()-1]));
        clientes[clientes.size()-1]->imprime();
      }

      else{
        clientes[clientes.size()-1]->imprime();
      sleep(2);
      a=1;  
      }
  }

  else{
    menucadastroclientes(cpf);
    a=0;
  }

  return a;
}


void Menu:: menucadastroclientes(string cpf){
  string nome;
  cout<< "=============================="<<endl;
  cout<< "          DADOS "<<endl;
  cout<< "     nome: ";
  nome=getString();
  cout<< "     cpf: "<<cpf<<endl;
  cout<< "=============================="<<endl;
  set_cliente(nome,cpf);
}

void Menu:: set_cliente(string nome,string cpf){
  clientes.push_back(new Cliente(nome,cpf));
  cout<< ">>>>cliente "<< nome << " cadastrado!<<<<"<<endl;
  sleep(1);
}

bool Menu:: cadastroexiste(string arquivo,string teste){

  if(arquivo==teste+".txt"){
    cout<< ">>>>cliente encontrado<<<<"<< endl;
    sleep(1);

    return true;
  }
  cout<< ">>>>cliente nao encontrado<<<<"<< endl;
  return false;
}


void Menu:: compra(int conf){
  string codigo;
  string cont="s";
  int quant;
  vector <Categoria *> categ;

  cout<< "=============================="<<endl;
  cout<< "           VENDA"<< endl;

  while(cont=="s" || cont=="S"){
    cout<< "      codigo: ";
    codigo=getString();
    cout<< "      quantidade: ";
    quant=getInput<int>();
    cout<< endl;
    clientes[clientes.size()-1]->compracliente(codigo,quant);

    cout<<"\n>>continuar? [s/n]<<"<<endl;
    cont=getString();
  }
  limpaTela();

  clientes[clientes.size()-1]->final();

  if(clientes[clientes.size()-1]->comprafinal()) {
    cout<< "***compra finalizada***"<<endl;
    clientes[clientes.size()-1]->cadastrocliente();
    cout<< "pressione ENTER";
    codigo=getString();
    menuInicial();
  }

  else{
    cout<< "***falta de estoque***"<<endl;
    cout<<"pressione ENTER";
    codigo= getString();
    menuInicial();
  }

}

void Menu:: modoRecomendacao(){

    cout<< "=============================="<<endl;
    cout<< "         MODO RECOMENDACAO    "<<endl;
    cout<< "  ****apenas para socios*****" <<endl;
    cout<< "          cpf: ";
    
  string arquivo;
  string cpf;
  string b;
  // int a=0;
  clientes.push_back(new Cliente());

  cpf= getString();

  arquivo= clientes[clientes.size()-1]->conf_cpf(cpf);

  if(cadastroexiste(arquivo,"./arq/clientes/"+cpf)){
    clientes[clientes.size()-1]=clientes[clientes.size()-1]->rArquivo(arquivo);

      if(clientes[clientes.size()-1]->get_socio() || clientes[clientes.size()-1]->get_total()>=100){
        clientes.push_back(new Csocio(clientes[clientes.size()-1]));
        clientes[clientes.size()-1]->imprime();
      }
      else{
        clientes[clientes.size()-1]->imprime();
      sleep(2);
      // a=1;  
      }
  }

    cout<<"pressione ENTER para continuar";
    b=getString();
    menuInicial();
}