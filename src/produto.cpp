#include <iostream>
#include <string>
#include <fstream>
#include "produto.hpp"

using namespace std;

Produto:: Produto(){
    set_codigo("x");
    set_nome("x");
    set_quant(0);
    set_valor(0.0);
}

Produto:: Produto (string codigo, string nome, int quant, float valor){
    set_codigo(codigo);
    set_nome(nome);
    set_quant(quant);
    set_valor(valor);
}

Produto:: ~Produto(){
}

void Produto:: set_codigo(string codigo){
    this->codigo=codigo;
}
string Produto:: get_codigo(){
    return this->codigo;
}

void Produto:: set_nome(string nome){
    this->nome=nome;
}
string Produto:: get_nome(){
    return this->nome;
}

void Produto:: set_quant(int quant){
    this->quant=quant;
}
int Produto:: get_quant(){
    return this->quant;
}

void Produto:: set_valor(float valor){
    this->valor=valor;
}
float Produto:: get_valor(){
    return this->valor;
}

void Produto:: imprime(){
    cout<<"====================="<<endl;
    cout<< "codigo: "<< get_codigo() << endl;
    cout<< "nome: "<< get_nome() << endl;
    cout<< "quantidade: " << get_quant() << endl;
    cout<< "valor: " << get_valor() << endl;
    cout<< "====================="<< endl;
}

void Produto:: cadastro_produto(){
    ofstream arquivos;
    arquivos.open("./arq/produtos/"+get_codigo()+".txt",ios:: trunc);

    if (arquivos.is_open()){
        arquivos << get_codigo() << endl;
        arquivos << get_nome() << endl;
        arquivos << get_quant() << endl;
        arquivos << get_valor() << endl;
        arquivos.close();
    }
}

string Produto :: conf_produto(string codigo){
    ifstream arquivo;
    string a = "./arq/produtos/"+codigo+".txt";

    arquivo.open(a,ios::in);

    if(arquivo.is_open()){
        return a;
    }

    arquivo.close();
    return "cadastro nao encontrado";
}


Produto * Produto:: rArquivo(string arquivo){
    ifstream arquivoe;
    Produto * produtos = new Produto();
    string texto;
    int cont=0;
    int quant;
    float valor;

    arquivoe.open(arquivo,ios::in);
    if (arquivoe.is_open()){
        getline(arquivoe,texto);
        produtos->set_codigo(texto);
        cont++;
        getline(arquivoe,texto);
        produtos->set_nome(texto);
        cont++;
        getline(arquivoe,texto);
        quant=stoi(texto);
        produtos->set_quant(quant);
        cont++;
        getline(arquivoe,texto);
        valor=stof(texto);
        produtos->set_valor(valor);

        arquivoe.close();        

    }
    return produtos;
} 

bool Produto:: conf_estoque (int quant) {
    if (get_quant()>= quant){
        set_quant(get_quant()-quant);
        return true;
    }

    return false;
    
}